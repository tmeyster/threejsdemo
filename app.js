var camera;
var scene;
var renderer;
var mesh;


init();
animate();

function init() {

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 1000;

    var light = new THREE.DirectionalLight( 0xffffff );
    light.position.set( 0, 1, 1 ).normalize();
    scene.add(light);

    var material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('NC_logo.jpg') } );

    var plane = new THREE.Mesh(new THREE.PlaneGeometry(800, 275), material);
    // plane.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 25, 0 ) );
    // mesh = new THREE.Mesh(geometry,  material);
    plane.doubleSided = true;
    plane.position.x = 0;
    plane.position.y = 0;
    plane.position.z = 0;
    // plane.rotation.z = ;
    // plane.rotation.x = -295;
    // plane.position.z = -90;
    // var materialImg = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('newNC_logo.png') } );
    var materialImg = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('newNC_logo.png'), transparent: true, opacity: .28 } );

    var objectImg = new THREE.Mesh(new THREE.PlaneGeometry(512, 510), materialImg);
    // objectImg.scale.set(.44, .44, 1);
    objectImg.scale.x = .45;
    objectImg.scale.y = .45;
    objectImg.scale.z = 1;
    console.log("objectImg: ", objectImg)
    // objectImg.material.opacity = 4.1;
    objectImg.position.x = -365;
    objectImg.position.y = 95;
    objectImg.position.z = 125;
    objectImg.rotation.x = 295;
    objectImg.rotation.z = 45;


    scene.add( plane );
    scene.add( objectImg);

    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    window.addEventListener( 'resize', onWindowResize, false );

    render();
}

function animate() {
    // mesh.rotation.x += .04;
    // mesh.rotation.y += .02;

    render();
    requestAnimationFrame( animate );
}

function render() {
    renderer.render( scene, camera );
}


function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    render();
}
